#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QVBoxLayout>
#include<QtGui>
#include<QLabel>
#include<QHBoxLayout>
#include<QFormLayout>
#include<QSpacerItem>
#include<QFrame>
#include<QCoreApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    this->setFixedSize(800, 300);

    QWidget* centralWidget = new QWidget();

    //buttons
    // QSpacerItem topSpacer = new QSpacerItem(2, 2, );
    m_copyButton = new QPushButton("&Copy ->", centralWidget);
    m_deleteButton = new QPushButton("&Delete", centralWidget);
    m_editButton = new QPushButton("&Edit...", centralWidget);
    m_newButton = new QPushButton("&New...", centralWidget);

    QVBoxLayout *buttonsLayout = new QVBoxLayout;
    buttonsLayout->insertStretch(0, 2);
    buttonsLayout->addWidget(m_copyButton);
    buttonsLayout->addWidget(m_deleteButton);
    buttonsLayout->addWidget(m_editButton);
    buttonsLayout->addWidget(m_newButton);
    // buttonsLayout->insertStretch(-1, 2);
    //

    m_browseButton = new QPushButton("Browse...", centralWidget);
    m_browseButton->setFixedWidth(m_newButton->width());

    QLabel* currentListLabel = new QLabel("\nC&urrent List", centralWidget);
    this->m_currentList = new QListWidget(centralWidget);
    currentListLabel->setBuddy(this->m_currentList);
    new QListWidgetItem("Edsger Dijkstra", this->m_currentList);

    QVBoxLayout *currentListLayout = new QVBoxLayout;
    currentListLayout->addWidget(currentListLabel);
    currentListLayout->addWidget(this->m_currentList);

    QLabel* masterListLabel = new QLabel("Sources avaliable in:\nMaster List", centralWidget);
    this->m_masterList = new QListWidget(centralWidget);
    currentListLabel->setBuddy(this->m_masterList);

    QHBoxLayout *masterListLayout = new QHBoxLayout;
    masterListLayout->addWidget(masterListLabel);
    masterListLayout->addWidget(this->m_browseButton);

    QVBoxLayout *leftListLayout = new QVBoxLayout;
    leftListLayout->addLayout(masterListLayout);
    leftListLayout->addWidget(this->m_masterList);

    QFrame *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(1);

    QHBoxLayout* secondPartLayout = new QHBoxLayout(centralWidget);
    secondPartLayout->addLayout(leftListLayout);
    secondPartLayout->addLayout(buttonsLayout);
    secondPartLayout->addLayout(currentListLayout);

    QVBoxLayout* mainLayout = new QVBoxLayout(centralWidget);
    mainLayout->addLayout(secondPartLayout);

    myLabelText = new QLabel ("Ayylmao");

    centralWidget->setLayout(mainLayout);
    this->setCentralWidget(centralWidget);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->type()==QKeyEvent::KeyPress)
    if(event->matches(QKeySequence::Copy))
    {
        QCoreApplication::quit();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
