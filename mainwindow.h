#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include<QPushButton>
#include<QListWidget>
#include<QKeyEvent>
#include<QLabel>

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QLabel *myLabelText;
    void keyPressEvent(QKeyEvent *event);

    QPushButton* m_copyButton;
    QPushButton* m_deleteButton;
    QPushButton* m_editButton;
    QPushButton* m_newButton;

    QPushButton* m_browseButton;

    QListWidget* m_currentList;
    QListWidget* m_masterList;

    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
